/*
 
 Author       :	 Waimun Yeow
 Date         :	 5/21/2010
 Description  :  An implementation of HugeInteger.h Header file functions.
 
 
 */


#include "HugeInteger.h"

#include <stdlib.h>
#include <iostream>

HugeInteger::HugeInteger()
{
	// initialize digits array to all zeros
	for (int i = 0; i < MAX_DIGITS; i++) {
		digits[i] = 0;
	}
	
	numberOfDigits = 1; // default value
	
	numberString = "0"; // initial value
	
	isNegativeNumber = false; // initial value
}

void HugeInteger::setNumber(string number)
{	
	/* perform input validation; checkNumber returns false if string
	 is not properly formatted as a number.
	 */
	if (!checkNumber(number)) {
		cout << "Invalid number format: " << number << ". HugeInteger defaults to 0." << endl;
		return;
	}
		
	// determine sign of number
	isNegativeNumber = number[0] == '-';
	
	// populate digits array with digits from number string; least significant digit first.
	
	int k = 0;
	if (isNegativeNumber) {
		k = number.length() - (number.length() - 1);
	}
	
	for (int i = number.length() - 1, j = MAX_DIGITS - 1; i >= k; i--) {
		setDigit(number[i] - '0', j);
		--j;
	}
}

string HugeInteger::toString()
{
	return isNegativeNumber ? "-" + numberString : numberString;
}

bool HugeInteger::isEqualsTo(HugeInteger number)
{
	return this->toString().compare(number.toString()) == 0;
}

bool HugeInteger::isGreaterThan(HugeInteger number)
{
	// return false if the two numbers are equal
	if (this->isEqualsTo(number)) {
		return false;
	}
	
	// compare signs of two numbers
	if (!this->isNegativeNumber && number.isNegativeNumber) {
		return true;
	} else if (this->isNegativeNumber && !number.isNegativeNumber) {
		return false;
	}
	
	// compare two positive numbers of different lengths
	if (!this->isNegativeNumber && !number.isNegativeNumber && this->numberOfDigits != number.numberOfDigits) {
		if (this->numberOfDigits > number.numberOfDigits) {
			return true;
		} else {
			return false;
		}
	}
	
	// compare two negative numbers of different lengths
	if (this->isNegativeNumber && number.isNegativeNumber && this->numberOfDigits != number.numberOfDigits) {
		if (this->numberOfDigits < number.numberOfDigits) {
			return true;
		} else {
			return false;
		}
	}
	
	// comparison of numbers with equal length as follows
	
	// obtain array index position of most significant digit
	int posMSD = MAX_DIGITS - numberOfDigits;
	
	bool compareResult = false;
	
	for (int i = posMSD; i < MAX_DIGITS; i++) {
		if (this->digits[i] == number.digits[i]) {
			continue;
		}
		
		compareResult = this->digits[i] > number.digits[i];
		break;
	}
	
	// two positive numbers yield compareResult
	if (!this->isNegativeNumber && !number.isNegativeNumber) {
		return compareResult;
	}
	
	// two negative numbers yield inverse of compareResult
	if (this->isNegativeNumber && number.isNegativeNumber) {
		return !compareResult;
	}
	
	cout << "Error: Program assertion failed. Returning false." << endl;
	return false;
}

bool HugeInteger::isLesserThan(HugeInteger number)
{
	// return false if the two numbers are equal
	if (this->isEqualsTo(number)) {
		return false;
	}
	
	return !this->isGreaterThan(number);
}

HugeInteger& HugeInteger::operator+=(const HugeInteger &other)
{
	// make copies
	HugeInteger lhs	= *this;
	HugeInteger rhs = other;
	
	HugeInteger result; // initial value of 0
	
	/*
	 perform regular addition if both numbers are positive or negative.
	 if both are negative, add negative sign to the result after add.
	 */
	if (lhs.isNegativeNumber == rhs.isNegativeNumber) {
		// do normal add
		result = add(lhs, rhs);
		
		if (lhs.isNegativeNumber && rhs.isNegativeNumber) {
			result.isNegativeNumber = true;
		}
	} else {
		result = subtract(lhs, rhs);
		
		if (lhs.isNegativeNumber) {
			HugeInteger positive = lhs;
			positive.isNegativeNumber = false;
			if (positive.isGreaterThan(rhs)) {
				result.isNegativeNumber = true;
			}
		} else {
			HugeInteger positive = rhs;
			positive.isNegativeNumber = false;
			if (positive.isGreaterThan(lhs)) {
				result.isNegativeNumber = true;
			}
		}
	}
	
	*this = result;
	return *this;
}

HugeInteger& HugeInteger::operator-=(const HugeInteger &other)
{
	// make copies
	HugeInteger lhs	= *this;
	HugeInteger rhs = other;
	
	HugeInteger result; // initial value of 0
	
	if (lhs.isNegativeNumber == rhs.isNegativeNumber) {
		result = subtract(lhs, rhs);
		
		if (lhs.isLesserThan(rhs)) {
			result.isNegativeNumber = true;
		}
	} else if (!lhs.isNegativeNumber && rhs.isNegativeNumber) {
		result = add(lhs, rhs);
	} else if (lhs.isNegativeNumber && !rhs.isNegativeNumber) {
		result = add(lhs, rhs);
		result.isNegativeNumber = true;
	}
	
	*this = result;
	return *this;
}

HugeInteger& HugeInteger::operator*=(const HugeInteger &other)
{
	// make copies
	HugeInteger lhs	= *this;
	HugeInteger rhs = other;
	
	HugeInteger result; // initial value of 0
	
	// make rhs value (multiplier) positive
	HugeInteger unsignedMultiplier = rhs;
	unsignedMultiplier.isNegativeNumber = false;
	
	int multiplier = atoi(unsignedMultiplier.toString().c_str());
	
	// multiplier permissible values -100 to 100
	if (multiplier < 0 || multiplier > 100) {
		cout << "Error: multiplier value must be between -100 and 100. Returning original left operand value." << endl;
		return *this;
	}
	
	/*
	 if multiplier is zero, return the result as zero.
	 if multiplier is one, the result will be the lhs value, and
	 the sign of the result will be determined.
	 */
	if (multiplier == 0 || lhs.isEqualsTo(result)) {
		*this = result;
		return *this;
	} else if (multiplier == 1) {
		if (lhs.isNegativeNumber != rhs.isNegativeNumber) {
			lhs.isNegativeNumber = true;
			*this = lhs;
			return *this;
		} else {
			lhs.isNegativeNumber = false;
			*this = lhs;
			return *this;
		}
	}
	
	// determine the sign of the result prior to multiplication
	bool resultIsNegative = lhs.isNegativeNumber != rhs.isNegativeNumber;
	
	// make lhs number positive
	lhs.isNegativeNumber = false;
	
	// assign result to lhs value; implying multiplier=1
	result = lhs;
	
	// multiply: loop through n times by addition
	for (int i = 2; i <= multiplier; i++) {
		result += lhs;
	}
	
	// assign sign back to final result
	result.isNegativeNumber = resultIsNegative;
	
	*this = result;
	return *this;
}

const HugeInteger HugeInteger::operator+(const HugeInteger &other) const
{
	HugeInteger result = *this; // Make a copy of myself.
	result += other; // Use += to add other to the copy.
	return result;
}

const HugeInteger HugeInteger::operator-(const HugeInteger &other) const
{
	HugeInteger result = *this; // Make a copy of myself.
	result -= other; // Use -= to add other to the copy.
	return result;
}

const HugeInteger HugeInteger::operator*(const HugeInteger &other) const
{
	HugeInteger result = *this; // Make a copy of myself.
	result *= other; // Use *= to add other to the copy.
	return result;
}

bool HugeInteger::checkNumber(string number)
{
	bool result = true; // number is valid
	
	// number string must not be empty
	if (number.empty()) {
		return false;
	}
	
	// number string cannot exceed max number of digits allowed
	if (number[0] == '-' && number.length() >= MAX_DIGITS + 2) {
		return false;
	} else if (number[0] != '-' && number.length() > MAX_DIGITS) {
		return false;
	}
	
	// number string cannot contain a negative sign without a digit
	if (number.length() == 1 && number[0] == '-') {
		return false;
	}
	
	// invalid: "-0" preceding number string
	if (number.length() >= 2 && number[0] == '-' && number[1] == '0') {
		return false;
	}
	
	// invalid: "0" preceding number string of 2 digits or more
	if (number.length() >= 2 && number[0] == '0') {
		return false;
	}
	
	for (int i = 0; i < number.length(); i++) {
		if (i == 0) {
			result = ((number[i] <= '9' && number[i] >= '0') || number[i] == '-');
		} else {
			result = (number[i] <= '9' && number[i] >= '0');
		}
		
		if (!result) {
			return false;
		}
	}
	
	return result;
}

void HugeInteger::setDigit(int digit, int arrayIndex)
{
	// check constraints of input digit; [0-9]
	if (!(digit >= 0 && digit <= 9)) {
		return;
	}
	
	// check arrayIndex to avoid array index out of bounds
	if (!(arrayIndex >= 0 && arrayIndex < MAX_DIGITS)) {
		return;
	}
	
	digits[arrayIndex] = digit; // set digit in digits array
	
	/*
	 update private field members: numberString and numberOfDigits
	 since the underlying backing store has been modified internally.
	 */
	
	string newNumberString = "";
	bool foundMSD = false;
	
	for (int i = 0; i < MAX_DIGITS; i++) {
		if (foundMSD) {
			newNumberString.push_back(digits[i] + '0');
			continue;
		}
		
		if (digits[i] != 0) {
			newNumberString.push_back(digits[i] + '0');
			foundMSD = true;
			// update length of huge integer
			numberOfDigits = MAX_DIGITS - i;
		}
	}
	
	// huge integer value is zero
	if (!foundMSD) {
		newNumberString = "0";
		numberOfDigits = 1;
	}
	
	numberString = newNumberString;
}

HugeInteger HugeInteger::add(HugeInteger firstNumber, HugeInteger secondNumber)
{
	HugeInteger result; // initial value of 0
	
	int posCarry = -1; // no carry-over
	
	/*
	 Performance:
	 instead of looping through the full length of the digits array, which
	 takes unnecessary time since both numbers might not be MAX_DIGITS long.
	 in order to find the loop index where looping will end, find the number
	 whose length is the longer than the other and subtract its length from
	 MAX_DIGITS. Example:
	 Number 1: 2893 (Length = 4) Number 2: 82 (Length = 2)
	 MSD position: 40 (MAX_DIGITS) - 4 (36, position of MSD of number '2893')
	 Loop index: 36 - 1 (offset to the left of MSD by 1 to allow for carry-over)
	 */
	int posMSD = 0; // the most significant digit of the larger number of the two
	
	if (firstNumber.numberOfDigits == secondNumber.numberOfDigits) {
		posMSD = MAX_DIGITS - firstNumber.numberOfDigits;
	} else if (firstNumber.numberOfDigits > secondNumber.numberOfDigits) {
		posMSD = MAX_DIGITS - firstNumber.numberOfDigits;
	} else {
		posMSD = MAX_DIGITS - secondNumber.numberOfDigits;
	}
	
	// add digits starting from the least to most significant digits (right to left)
	
	for (int i = MAX_DIGITS - 1, sum = 0; i >= posMSD - 1; i--) {
		// no carry-over set, sum of two zeros is zero, skip add logic
		if (firstNumber.digits[i] == 0 && secondNumber.digits[i] == 0 && posCarry == -1) {
			continue;
		}
		
		// if there is a carry-over, consider it in the sum
		if (posCarry == i) {
			sum = 1 + firstNumber.digits[i] + secondNumber.digits[i];
			
			if (sum < 10) {
				posCarry = -1; // reset; no carry-over
			} else {
				sum %= 10;
				posCarry = i - 1;
			}
		} else {
			sum = firstNumber.digits[i] + secondNumber.digits[i];
			
			// indicates carry-over
			if (sum > 9) {
				sum %= 10;
				posCarry = i - 1; // position of the next digit
			}
		}
		
		result.setDigit(sum, i); // store sum result
		
		// indicates carry-over, and note the position of the next digit
		if (sum == 0) {
			if (i != 0) {
				posCarry = i - 1;
			} else {
				cout << "Error: overflow during add arithmetic. Returning ??" << endl;
				return result;
			}
		}
	}
	
	return result;
}

HugeInteger HugeInteger::subtract(HugeInteger firstNumber, HugeInteger secondNumber)
{
	/*
	 Steps:
	 subtraction of two numbers ignores the signs, find the greater among the two
	 unsigned numbers, and performs the subtraction (greater - lesser). result obtained
	 will be unsigned; positive.
	 */
	
	HugeInteger unsignedNumbers[2] = {firstNumber, secondNumber};
	
	// ignore signs and make them positive numbers
	unsignedNumbers[0].isNegativeNumber = unsignedNumbers[0].isNegativeNumber ? false : false;
	unsignedNumbers[1].isNegativeNumber = unsignedNumbers[1].isNegativeNumber ? false : false;
	
	HugeInteger result; // initial value of 0
	
	// sort numbers in descending order
	if (unsignedNumbers[0].isLesserThan(unsignedNumbers[1])) {
		HugeInteger swap = unsignedNumbers[0];
		unsignedNumbers[0] = unsignedNumbers[1];
		unsignedNumbers[1] = swap;
	}
	
	// subtraction of two equal numbers yield zero
	if (unsignedNumbers[0].isEqualsTo(unsignedNumbers[1])) {
		return result;
	}
	
	/*
	 Performance:
	 instead of looping through the full length of the digits array, which
	 takes unncessary time since both numbers might not be MAX_DIGITS long.
	 in order to find the loop index where looping will end, find the number
	 whose length is the longer than the other and subtract its length from
	 MAX_DIGITS. Example:
	 Number 1: 2893 (Length = 4) Number 2: 82 (Length = 2)
	 MSD position: 40 (MAX_DIGITS) - 4 (36, position of MSD of number '2893')
	 Loop index: 36 - 1 (offset to the left of MSD by 1 to allow for borrowing)
	 */
	int posMSD = 0; // the most significant digit of the larger number of the two
	
	if (unsignedNumbers[0].numberOfDigits == unsignedNumbers[1].numberOfDigits) {
		posMSD = MAX_DIGITS - unsignedNumbers[0].numberOfDigits;
	} else if (unsignedNumbers[0].numberOfDigits > unsignedNumbers[1].numberOfDigits) {
		posMSD = MAX_DIGITS - unsignedNumbers[0].numberOfDigits;
	} else {
		posMSD = MAX_DIGITS - unsignedNumbers[1].numberOfDigits;
	}
	
	/*
	 subtract digits starting from the least to most significant digits (right to left)
	 */
	
	for (int i = MAX_DIGITS - 1, diff = 0; i >= posMSD - 1; i--) {
		// borrow 1 from the left of the current digit position
		if (unsignedNumbers[0].digits[i] < unsignedNumbers[1].digits[i] && unsignedNumbers[0].digits[i-1] !=0) {
			unsignedNumbers[0].setDigit(unsignedNumbers[0].digits[i-1] - 1, i-1); // borrow number
			
			// subtract
			diff = 10 + unsignedNumbers[0].digits[i] - unsignedNumbers[1].digits[i];
			
			// store diff result
			result.setDigit(diff, i);
		} else {
			// subtract
			diff = unsignedNumbers[0].digits[i] - unsignedNumbers[1].digits[i];
			
			// make diff positive
			if (diff < 0) {
				diff = -diff;
			}
			
			// store diff result
			result.setDigit(diff, i);
		}
	}
	
	return result;
}
