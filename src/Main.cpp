/*
 
 Author       :	 Waimun Yeow
 Date         :	 5/21/2010
 Description  :  Main program that takes in text file to read numbers for arithmetic calculations.
 
 
 */

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <iomanip>
#include <vector>
#include <stdio.h>

using namespace std;

#include "HugeInteger.cpp"

//main function can take parameters
//this allows for arguments on the command line for the program
int main(  int argc, const char* argv[]  )
{
    //input and output file names
	char fileName[256];
    char fileNameOut[256];
    
    //a single line of text 
    char currentLine[256];
    
    //get the file name from the command line
    if (argc == 2) //one argument, set fileName to the first argument
        strcpy(fileName, argv[1]);
    else
    {
        cerr <<"Error: file not found or invalid"
		"\tUsage: HugeInteger numbers.txt \n\n";
        return -1;
    }
    
    //Open file for input and verify that it opened
	ifstream fin (fileName);	//input stream from fileName
	if (!fin)					//check if input file 
	{							//was opened successfully
		cerr <<"Error: Cannot open the file "<< fileName <<"\n";
		return -1;
	}
	
	//construct the name of output file based on the input file
    strcpy(fileNameOut, fileName);
    strcat(fileNameOut,"_out");
    
    //Open file for output and verify that it opened
    ofstream fout(fileNameOut);	//output stream 
	if (!fout)					//check if output file 
	{							//was opened successfully
		cerr <<"Cannot open the file "<<fileNameOut <<"\n";
		return -1;
	}
	
    //read the content of the file one line at a time
    //writing output to the output file
    
    while ( !fin.eof() )
    {
        fin.getline(currentLine, 255);
		
		char *tokenPtr;
		char *tempCurrentLine = currentLine;
		
		vector <string> currentLineTokens;
		
		// begin tokenization of the current line
		tokenPtr = strtok (tempCurrentLine, " ");
		
		// continue tokenizing the line until tokenPtr becomes NULL
		while (tokenPtr != NULL) {	
			currentLineTokens.push_back(tokenPtr);
			tokenPtr = strtok (NULL, " "); //get next token
		}
		
		for (vector<string>::iterator i = currentLineTokens.begin(); i != currentLineTokens.end(); ++i) {
			string token = *i;
			if (token.compare("add")==0 && currentLineTokens.size() == 3) {
				HugeInteger a, b, result;
				a.setNumber(currentLineTokens[1]);
				b.setNumber(currentLineTokens[2]);
				result = a + b;
				fout << result.toString() << "\n";
			} else if (token.compare("sub")==0 && currentLineTokens.size() == 3) {
				HugeInteger a, b, result;
				a.setNumber(currentLineTokens[1]);
				b.setNumber(currentLineTokens[2]);
				result = a - b;
				fout << result.toString() << "\n";
			} else if (token.compare("mul")==0 && currentLineTokens.size() == 3) {
				HugeInteger a, b, result;
				a.setNumber(currentLineTokens[1]);
				b.setNumber(currentLineTokens[2]);
				result = a * b;
				fout << result.toString() << "\n";
			}
		}
    }
    
    //close the input and output files    
	fin.close();
	fout.close();
	
	return 0;
	
} //end main
