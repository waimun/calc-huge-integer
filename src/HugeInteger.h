/*
 
 Author       :	 Waimun Yeow
 Date         :	 5/21/2010
 Description  :  Contains functions for creating HugeInteger numbers, comparison operators, and arithmetic calculations.
 
 
 */


#include <string>

using namespace std;

class HugeInteger
{
public:
	/**
	 The maximum limit number of digits this HugeInteger can allow.
	 */
	const static int MAX_DIGITS = 40;
	
	/**
	 Default constructor; huge integer value defaults to 0.
	 */
	HugeInteger();
	
	/**
	 Sets the huge integer value using the specified number string.
	 */
	void setNumber(string number);
	
	/**
	 Returns the string representation of this huge integer.
	 */
	string toString();
	
	/**
	 Returns true if this huge integer is equal to the specified huge
	 integer; false otherwise.
	 */
	bool isEqualsTo(HugeInteger number);
	
	/**
	 Returns true if this huge integer is greater than the specified
	 huge integer; false otherwise.
	 */
	bool isGreaterThan(HugeInteger number);
	
	/**
	 Returns true if this huge integer is lesser than the specified
	 huge integer; false otherwise.
	 */
	bool isLesserThan(HugeInteger number);
	
	/**
	 Overloaded Compound Assignment Operator +=
	 C++ Operator Overloading Guidelines:
	 http://www.cs.caltech.edu/courses/cs11/material/cpp/donnie/cpp-ops.html
	 */
	HugeInteger& operator+=(const HugeInteger &other);
	
	/**
	 Overloaded Compound Assignment Operator -=
	 */
	HugeInteger& operator-=(const HugeInteger &other);
	
	/**
	 Overloaded Compound Assignment Operator *=
	 */
	HugeInteger& operator*=(const HugeInteger &other);
	
	/**
	 Overloaded Binary Arithmetic Operator +
	 */
	const HugeInteger operator+(const HugeInteger &other) const;
	
	/**
	 Overloaded Binary Arithmetic Operator -
	 */
	const HugeInteger operator-(const HugeInteger &other) const;
	
	/**
	 Overloaded Binary Arithmetic Operator *
	 */
	const HugeInteger operator*(const HugeInteger &other) const;
	
private:
	/**
	 Array to store all digits of the huge integer for computational
	 methods (add, subtract, etc).
	 */
	int digits[MAX_DIGITS];
	
	/**
	 Huge integer value stored as string.
	 */
	string numberString;
	
	/**
	 Number of digits that this huge integer value contains.
	 */
	int numberOfDigits;
	
	/**
	 Indicates if this huge integer is a negative number.
	 */
	bool isNegativeNumber;
	
	/**
	 Checks if the specified string is properly formatted as a number.
	 Examples: does not exceed max. number of digits allowed, no + sign,
	 no leading zeros.
	 Exceptions raised are logged to console.
	 */
	bool checkNumber(string number);
	
	/**
	 Sets the specified digit in the digits array index position.
	 */
	void setDigit(int digit, int arrayIndex);
	
	/**
	 Adds two huge integer numbers and returns a huge integer result.
	 This method will perform addition of unsigned numbers regardless
	 of the signs of the numbers passed in.
	 */
	HugeInteger add(HugeInteger firstNumber, HugeInteger secondNumber);
	
	/**
	 Subtracts two huge integer numbers and returns a huge integer result.
	 This method will perform subtraction of unsigned numbers regardless
	 of the signs of the numbers passed in.
	 */
	HugeInteger subtract(HugeInteger firstNumber, HugeInteger secondNumber);
};
